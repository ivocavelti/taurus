USE taurus;

INSERT INTO `taurus`.`tbl_person` (`name`, `lifetime`) VALUES 
	('René Descartes', '1596-1650'),
	('Gaius Julius Caesar', '100BC-44BC'),
	('Albert Einstein', '1879-1955'),
	('Aristoteles', '384BC-322BC'),
	('Homer', '810BC-744BC'),
	('Quintilian', '30-96'),
	('Bjarne Stroustrup', '1950-'),
	('Winston Churchill', '1864-1965'),
	('Mahatma Gandhi', '1869-1948'),
	('Linus Torvalds', '1969-'),
	('Unbekannt', 'Unbekannt'),
	('Dalai Lama XIV.', '1935-'),
	('Ronald Reagan', '1911-2004'),
	('Bertolt Brecht', '1898-1956');
INSERT INTO `taurus`.`tbl_quote` (`quote`, `correct_answer`) VALUES 
	('Cogito, ergo sum', (SELECT id_person FROM tbl_person WHERE name LIKE "René Descartes")),
	('Veni, vidi, vici', (SELECT id_person FROM tbl_person WHERE name LIKE "Gaius Julius Caesar")),
	('Phantasie ist wichtiger als Wissen, denn Wissen ist begrenzt.', (SELECT id_person FROM tbl_person WHERE name LIKE "Albert Einstein")),
	('Staunen ist der erste Grund der Philosophie.', (SELECT id_person FROM tbl_person WHERE name LIKE "Aristoteles")),
	('Wir sollten das Leben verlassen wie ein Bankett: Weder durstig noch betrunken.', (SELECT id_person FROM tbl_person WHERE name LIKE "Aristoteles")),
	('Gleich und gleich gesellt sich gern.', (SELECT id_person FROM tbl_person WHERE name LIKE "Homer")),
	('Praxis ohne Theorie leistet immer noch mehr als Theorie ohne Praxis.', (SELECT id_person FROM tbl_person WHERE name LIKE "Quintilian")),
	('C macht es einfach, sich selbst ins Bein zu schiessen; C++ erschwert es, aber wenn es dir gelingt, sprengt es dir das ganze Bein weg.', (SELECT id_person FROM tbl_person WHERE name LIKE "Bjarne Stroustrup")),
	('Ich glaube keiner Statistik, die ich nicht selbst gefälscht habe.', (SELECT id_person FROM tbl_person WHERE name LIKE "Winston Churchill")),
	('Eine Person, die nie einen Fehler gemacht hat, hat nie etwas Neues probiert.', (SELECT id_person FROM tbl_person WHERE name LIKE "Albert Einstein")),
	('Der Schwache kann nicht verzeihen. Verzeihen ist eine Eigenschaft des Starken.', (SELECT id_person FROM tbl_person WHERE name LIKE "Mahatma Gandhi")),
	('Programmiere deinen Code so, als sei der Typ, der den Code pflegen muss, ein gewaltbereiter Psychopath, der weiss, wo du wohnst.', (SELECT id_person FROM tbl_person WHERE name LIKE "Unbekannt")),
	('Software ist wie Sex: Sie ist besser, wenn sie gratis ist.', (SELECT id_person FROM tbl_person WHERE name LIKE "Linus Torvalds")),
	('Ich bin ein Berliner!', (SELECT id_person FROM tbl_person WHERE name LIKE "Ronald Reagan")),
	('Wer die Wahrheit nicht weiss, der ist bloss ein Dummkopf.', (SELECT id_person FROM tbl_person WHERE name LIKE "Bertolt Brecht"));