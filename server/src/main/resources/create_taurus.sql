DROP DATABASE IF EXISTS taurus;

CREATE DATABASE taurus;
USE taurus;

CREATE TABLE tbl_person (
	id_person INT AUTO_INCREMENT,
	name VARCHAR(255) NOT NULL,
	lifetime VARCHAR(255) NOT NULL,
	PRIMARY KEY (id_person)
);

CREATE TABLE tbl_quote (
	id_quote INT AUTO_INCREMENT,
	quote VARCHAR(255) NOT NULL,
	correct_answer INT NOT NULL,
	PRIMARY KEY (id_quote),
	FOREIGN KEY (correct_answer) REFERENCES tbl_person(id_person)
);

CREATE TABLE tbl_user (
	id_user INT AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL UNIQUE,
	password VARCHAR(255) NOT NULL,
	PRIMARY KEY (id_user)
);

CREATE TABLE tbl_highscores (
	id_highscore INT AUTO_INCREMENT,
	username VARCHAR(255) NOT NULL,
	points BIGINT NOT NULL,
	PRIMARY KEY (id_highscore)
);