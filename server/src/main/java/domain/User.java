package domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

import java.security.Principal;
import java.util.Objects;

@AllArgsConstructor
@Builder
@Data
public class User {
    private String username;
    private String encryptedPassword;
}
