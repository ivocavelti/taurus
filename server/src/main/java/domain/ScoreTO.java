package domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class ScoreTO {
    private String username;
    private long score;
}
