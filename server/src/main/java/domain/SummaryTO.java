package domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SummaryTO {
    private String username;
    private int question;
    private int totalQuestions;
    private long points;
}
