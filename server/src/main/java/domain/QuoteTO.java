package domain;

import lombok.Builder;

@Builder
public class QuoteTO {
    private int quoteId;
    private String quote;
    private QuoterTO[] quoters;
}
