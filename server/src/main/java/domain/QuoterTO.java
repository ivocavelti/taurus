package domain;

import lombok.AllArgsConstructor;
import lombok.Builder;

@Builder
@AllArgsConstructor
public class QuoterTO {
    private int id;
    private String name;
}
