package domain;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class AnsweredQuote {
    private int quoteId;
    private int correctAnswer;
    private long startTime;
    private long endTime;
    private long points;
}
