import com.google.gson.Gson;
import domain.*;
import exception.UnauthorizedUserException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.jooq.generated.tables.pojos.TblUser;
import services.GameService;
import services.HighScoreService;
import services.QuoteService;
import services.UserService;
import spark.Request;
import spark.Response;

import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
class ApplicationAdapter {

    private final QuoteService quoteService;
    private final UserService userService;
    private final GameService gameService;
    private final HighScoreService highScoreService;
    private Gson gson = new Gson();

    public static final String USERNAME_IN_SESSION = "username";

    QuoteTO getRandomQuote(Request req, Response res) {
        res.type("application/json");
        String username = authenticate(req);
        return quoteService.getRandomQuote(username);
    }

    public Object logIn(Request request, Response response) {
        UserTO userTO = gson.fromJson(request.body(), UserTO.class);
        TblUser user = userService.handleLogInPost(userTO);
        request.session().attribute(USERNAME_IN_SESSION, user.getUsername());
        return "";
    }

    public Object registerUser(Request request, Response response) {
        UserTO user = gson.fromJson(request.body(), UserTO.class);
        userService.registerUser(user);
        return "";
    }

    private String authenticate(Request request) {
        Optional<String> username = Optional.ofNullable(
                request.session().attribute(ApplicationAdapter.USERNAME_IN_SESSION)
        );
        return username.orElseThrow(() -> new UnauthorizedUserException("User isn't logged in"));
    }

    public Object logOut(Request request, Response response) {
        request.session().removeAttribute(USERNAME_IN_SESSION);
        return "";
    }

    public SummaryTO startGame(Request request, Response response) {
        String username = authenticate(request);
        return gameService.startGame(username);
    }

    public SummaryTO answerQuote(Request request, Response response) {
        String username = authenticate(request);
        int answer = Integer.parseInt(request.params("answer"));
        return gameService.answerQuote(username, answer);
    }

    public List<ScoreTO> getHighScore(Request request, Response response) {
        authenticate(request);
        return highScoreService.getHighScores();
    }

    public SimpleUserTO getUser(Request request, Response response) {
        String username = authenticate(request);
        return new SimpleUserTO(username);
    }
}
