import com.google.gson.Gson;

import static spark.Spark.*;

public class MainApplication {
    public static void main(String[] args) {
        Application application = new Application();
        application.run();
    }
}
