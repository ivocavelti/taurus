package repositories;

import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.generated.tables.pojos.TblHighscores;

import java.util.List;

import static org.jooq.generated.Tables.TBL_HIGHSCORES;

@RequiredArgsConstructor
public class HighScoreRepository {

    private final DSLContext dslContext;

    public void addScore(String username, long points) {
        dslContext
                .insertInto(TBL_HIGHSCORES, TBL_HIGHSCORES.USERNAME, TBL_HIGHSCORES.POINTS)
                .values(username, points)
                .execute();
    }

    public List<TblHighscores> getScores() {
        return dslContext
                .selectFrom(TBL_HIGHSCORES)
                .fetchInto(TblHighscores.class);
    }
}
