package repositories;

import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.generated.tables.pojos.TblQuote;

import java.util.List;

import static org.jooq.generated.Tables.TBL_QUOTE;

@RequiredArgsConstructor
public class QuoteRepository {

    private final DSLContext context;

    public List<TblQuote> getQuotes() {
        return context
                .selectFrom(TBL_QUOTE)
                .fetchInto(TblQuote.class);
    }
}
