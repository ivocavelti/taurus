package repositories;

import domain.User;
import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.generated.tables.pojos.TblUser;

import java.util.Optional;

import static org.jooq.generated.tables.TblUser.TBL_USER;

@RequiredArgsConstructor
public class UserRepository {

    private final DSLContext dslContext;

    public void registerUser(User user) {
        dslContext
                .insertInto(TBL_USER, TBL_USER.USERNAME, TBL_USER.PASSWORD)
                .values(user.getUsername(), user.getEncryptedPassword())
                .execute();
    }

    public Optional<TblUser> getUserWithName(String username) {
        return dslContext
                .selectFrom(TBL_USER)
                .where(TBL_USER.USERNAME.eq(username))
                .fetchOptionalInto(TblUser.class);
    }
}
