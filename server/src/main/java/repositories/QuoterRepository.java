package repositories;

import lombok.RequiredArgsConstructor;
import org.jooq.DSLContext;
import org.jooq.generated.tables.pojos.TblPerson;

import java.util.List;

import static org.jooq.generated.Tables.TBL_PERSON;

@RequiredArgsConstructor
public class QuoterRepository {

    private final DSLContext context;

    public List<TblPerson> getQuoters() {
        return context
                .selectFrom(TBL_PERSON)
                .fetchInto(TblPerson.class);
    }
}