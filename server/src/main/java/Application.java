import com.google.gson.Gson;
import domain.ErrorTO;
import exception.BadRequestException;
import exception.UnauthorizedUserException;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import repositories.HighScoreRepository;
import repositories.QuoteRepository;
import repositories.QuoterRepository;
import repositories.UserRepository;
import services.GameService;
import services.HighScoreService;
import services.QuoteService;
import services.UserService;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import static spark.Spark.*;

class Application {

    private final static String BASE_URL = "/api";
    private final static String QUOTE_URL = BASE_URL + "/quote";
    private final static String ANSWER_QUOTE_URL = QUOTE_URL + "/:answer";

    private static final String START_GAME_URL = BASE_URL + "/game/start";

    private static final String LOG_IN_URL = BASE_URL + "/log-in";
    private static final String LOG_OUT_URL = BASE_URL + "/log-out";
    private static final String REGISTER_URL = BASE_URL + "/register";
    private static final String HIGH_SCORES_URL = BASE_URL + "/highscores";
    private static final String GET_USER_URL = BASE_URL + "/user";

    void run() {
        ApplicationAdapter applicationAdapter = getApplicationAdapter();
        port(8080);
        Gson gson = new Gson();
        get(QUOTE_URL, applicationAdapter::getRandomQuote, gson::toJson);
        post(ANSWER_QUOTE_URL, applicationAdapter::answerQuote, gson::toJson);
        post(START_GAME_URL, applicationAdapter::startGame, gson::toJson);

        get(HIGH_SCORES_URL, applicationAdapter::getHighScore, gson::toJson);

        get(GET_USER_URL, applicationAdapter::getUser, gson::toJson);
        post(LOG_IN_URL, applicationAdapter::logIn);
        post(REGISTER_URL, applicationAdapter::registerUser);
        post(LOG_OUT_URL, applicationAdapter::logOut);

        exception(UnauthorizedUserException.class, (exception, request, response) -> {
            response.type("application/json");
            response.status(401);
            response.body(gson.toJson(new ErrorTO(exception.getMessage())));
        });

        exception(BadRequestException.class, (exception, request, response) -> {
            response.type("application/json");
            response.status(400);
            response.body(gson.toJson(new ErrorTO(exception.getMessage())));
        });
    }

    private ApplicationAdapter getApplicationAdapter() {
        DSLContext context = getDatabaseConnection();
        QuoteRepository quoteRepository = new QuoteRepository(context);
        QuoterRepository quoterRepository = new QuoterRepository(context);
        HighScoreRepository highScoreRepository = new HighScoreRepository(context);
        HighScoreService highScoreService = new HighScoreService(highScoreRepository);
        GameService gameService = new GameService(highScoreService);
        QuoteService quoteService = new QuoteService(quoteRepository, quoterRepository, gameService);
        UserRepository userRepository = new UserRepository(context);
        UserService userService = new UserService(userRepository);
        return new ApplicationAdapter(quoteService, userService, gameService, highScoreService);
    }

    private DSLContext getDatabaseConnection() {
        String userName = "root";
        String password = "";
        String url = "jdbc:mysql://localhost:3306/taurus?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
        try {
            Connection conn = DriverManager.getConnection(url, userName, password);
            return DSL.using(conn, SQLDialect.MYSQL);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
