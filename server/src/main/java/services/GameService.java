package services;

import domain.AnsweredQuote;
import domain.SummaryTO;
import exception.BadRequestException;
import lombok.RequiredArgsConstructor;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Stack;

@RequiredArgsConstructor
public class GameService {

    private final HighScoreService highScoreService;
    private Map<String, Stack<AnsweredQuote>> runningGames = new HashMap<>();

    private static final long START_POINTS = 40000;
    private static final int TOTAL_QUESTIONS = 10;

    public SummaryTO startGame(String username) {
        runningGames.put(username, new Stack<>());
        return getSummary(username);
    }

    public void addQuote(String username, int idQuote, int correctAnswer) {
        AnsweredQuote answeredQuote = AnsweredQuote
                .builder()
                .quoteId(idQuote)
                .correctAnswer(correctAnswer)
                .startTime(System.currentTimeMillis())
                .build();
        Stack<AnsweredQuote> stack = Optional.ofNullable(runningGames.get(username))
                .orElseThrow(() -> new BadRequestException("Game hasn't started yet."));
        if (stack.size() >= TOTAL_QUESTIONS) {
            throw new BadRequestException("Reached maximal questions");
        }
        stack.push(answeredQuote);
    }

    public SummaryTO answerQuote(String username, int guess) {
        AnsweredQuote quote = runningGames.get(username).peek();
        quote.setEndTime(System.currentTimeMillis());
        if (quote.getCorrectAnswer() == guess) {
            quote.setPoints(calcPoints(quote.getStartTime(), quote.getEndTime()));
        } else {
            quote.setPoints(0);
        }
        SummaryTO summary = getSummary(username);
        if (runningGames.get(username).size() >= TOTAL_QUESTIONS) {
            highScoreService.addScore(username, summary.getPoints());
        }
        return summary;
    }

    private SummaryTO getSummary(String username) {
        Stack<AnsweredQuote> stack = new Stack<>();
        stack.addAll(runningGames.get(username));
        return SummaryTO
                .builder()
                .question(stack.size())
                .totalQuestions(TOTAL_QUESTIONS)
                .username(username)
                .points(totalPoints(stack))
                .build();
    }

    private long totalPoints(Stack<AnsweredQuote> stack) {
        long sum = 0;
        while (stack.size() > 0) sum += stack.pop().getPoints();
        return sum;
    }

    private long calcPoints(long startTime, long endTime) {
        long diff = endTime - startTime;
        long points = START_POINTS - diff;
        return points > 0 ? points : 0;
    }
}
