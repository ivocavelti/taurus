package services;

import at.favre.lib.crypto.bcrypt.BCrypt;
import domain.User;
import domain.UserTO;
import exception.BadRequestException;
import exception.UnauthorizedUserException;
import lombok.RequiredArgsConstructor;
import org.jooq.exception.DataAccessException;
import org.jooq.generated.tables.pojos.TblUser;
import repositories.UserRepository;

@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private static final String USERNAME_REGEX_PATTERN = "^.{1,20}";

    public TblUser handleLogInPost(UserTO userTO) {
        TblUser user = userRepository.getUserWithName(userTO.getUsername())
                .orElseThrow(() -> new UnauthorizedUserException("There's no user with this username."));
        BCrypt.Result result = BCrypt.verifyer()
                .verify(userTO.getPassword().toCharArray(), user.getPassword());
        if(!result.verified) {
            throw new UnauthorizedUserException("Passwort does not match username.");
        }
        return user;
    }

    public void registerUser(UserTO userTO) {
        validateUser(userTO);
        String password = userTO.getPassword();
        String passwordBCryptHash = BCrypt.withDefaults().hashToString(12, password.toCharArray());
        User user = new User(userTO.getUsername(), passwordBCryptHash);
        try {
            userRepository.registerUser(user);
        } catch (DataAccessException e) {
            throw new UnauthorizedUserException("User already exists");
        }
    }

    void validateUser(UserTO userTO) {
        if (!(userTO.getPassword().length() >= 8)) {
            throw new BadRequestException("Password has to be longer than 8 digits.");
        } else if (!userTO.getUsername().matches(USERNAME_REGEX_PATTERN)) {
            throw new BadRequestException("Username has to be between 1 and 20 chars");
        }
    }

}
