package services;

import domain.ScoreTO;
import lombok.RequiredArgsConstructor;
import repositories.HighScoreRepository;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class HighScoreService {

    private final HighScoreRepository highScoreRepository;

    public void addScore(String username, long score) {
        highScoreRepository.addScore(username, score);
    }

    public List<ScoreTO> getHighScores() {
        return highScoreRepository.getScores()
                .stream()
                .map(score -> ScoreTO
                        .builder()
                        .username(score.getUsername())
                        .score(score.getPoints())
                        .build())
                .sorted((o1, o2) -> Math.toIntExact(o2.getScore() - o1.getScore()))
                .collect(Collectors.toList());
    }
}
