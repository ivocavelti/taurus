package services;

import domain.QuoteTO;
import domain.QuoterTO;
import lombok.RequiredArgsConstructor;
import org.jooq.generated.tables.pojos.TblPerson;
import org.jooq.generated.tables.pojos.TblQuote;
import repositories.QuoteRepository;
import repositories.QuoterRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
public class QuoteService {

    private static final int AMOUNT_OF_OPTIONS = 4;
    private static final int FIRST_LIST_ENTRY = 0;

    private final QuoteRepository quoteRepository;
    private final QuoterRepository quoterRepository;
    private final GameService gameService;

    public QuoteTO getRandomQuote(String username) {
        List<TblQuote> allQuotes = quoteRepository.getQuotes();
        Collections.shuffle(allQuotes);
        TblQuote tblQuote = allQuotes.get(FIRST_LIST_ENTRY);
        gameService.addQuote(username, tblQuote.getIdQuote(), tblQuote.getCorrectAnswer());
        return QuoteTO
                .builder()
                .quoteId(tblQuote.getIdQuote())
                .quote(tblQuote.getQuote())
                .quoters(getRandomQuoters(tblQuote.getCorrectAnswer()))
                .build();
    }

    private QuoterTO[] getRandomQuoters(Integer quoterId) {
        List<QuoterTO> quoters = new ArrayList<>();
        List<TblPerson> allQuoters = quoterRepository.getQuoters();
        // Get the correct answer from the list
        TblPerson quoter = getQuoter(quoterId, allQuoters);
        quoters.add(tblPersonToQuoterTO(quoter));
        // Remove the correct answer from the list so it can't be used twice
        allQuoters.remove(quoter);
        Collections.shuffle(allQuoters);
        for (int i = 1; i < AMOUNT_OF_OPTIONS; i++) {
            quoters.add(tblPersonToQuoterTO(allQuoters.get(i)));
        }
        Collections.shuffle(quoters);
        return quoters.toArray(new QuoterTO[]{});
    }

    private TblPerson getQuoter(Integer quoterId, List<TblPerson> allQuoters) {
        Optional<TblPerson> quoter = allQuoters
                .stream()
                .filter(entry -> entry.getIdPerson().equals(quoterId))
                .reduce((a, b) -> null);
        return quoter.orElseThrow(() -> new RuntimeException("Couldn't find quoter with id " + quoterId));
    }

    private QuoterTO tblPersonToQuoterTO(TblPerson quoter) {
        return QuoterTO
                .builder()
                .id(quoter.getIdPerson())
                .name(quoter.getName())
                .build();
    }

}
