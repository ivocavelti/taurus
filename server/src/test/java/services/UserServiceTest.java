package services;

import domain.UserTO;
import repositories.UserRepository;

import static org.mockito.Mockito.mock;

public class UserServiceTest {

    @org.junit.Test
    public void validateUserShouldBeValid() {
        UserTO userTO = UserTO
                .builder()
                .username("Woogel")
                .password("12345678")
                .build();
        UserService userService = new UserService(mock(UserRepository.class));

        userService.validateUser(userTO);
    }
}