# Taurus

## Let it Run

1. Type database credentials in Application.java and build.gradle
2. Run the database scripts in "resources".
2. Run the server with `./gradlew run`
3. Run the client with `npm run start`
4. Visit `http://localhost:4200`