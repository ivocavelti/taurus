import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "../domain/user";
import {Router, UrlSerializer} from "@angular/router";
import {Error} from "../domain/error";

@Component({
  selector: 'app-log-in',
  templateUrl: './log-in.component.html',
  styleUrls: ['./log-in.component.scss']
})
export class LogInComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  user = new class implements User {
    password: string;
    username: string;
  };

  error: Error;

  ngOnInit() {
  }

  logIn() {
    this.httpClient.post<Error>("/api/log-in", this.user)
      .subscribe((data => this.router.navigate(["/"])),
        error => this.error = error.error);
  }

  register() {
    this.httpClient.post<Error>("/api/register", this.user)
      .subscribe((data => this.router.navigate(["/"])),
        error => this.error = error.error);
  }
}
