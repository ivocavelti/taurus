import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {HighscoresComponent} from './highscores/highscores.component';
import {GameComponent} from './game/game.component';
import {HomeComponent} from './home/home.component';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MatButtonModule, MatFormFieldModule, MatInputModule, MatMenuModule, MatTableModule} from "@angular/material";
import {HttpClientModule} from "@angular/common/http";
import {RouterModule, Routes} from "@angular/router";
import {LogInComponent} from './log-in/log-in.component';
import {FormsModule} from "@angular/forms";

const appRoutes: Routes = [
  {path: 'highscores', component: HighscoresComponent},
  {path: 'log-in', component: LogInComponent},
  {path: 'game', component: GameComponent},
  {path: '', component: HomeComponent, pathMatch: 'full'},

];

@NgModule({
  declarations: [
    AppComponent,
    HighscoresComponent,
    GameComponent,
    HomeComponent,
    LogInComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatTableModule,
    HttpClientModule,
    MatFormFieldModule,
    MatInputModule,
    MatMenuModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
