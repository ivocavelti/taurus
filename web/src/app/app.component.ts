import {Component, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {User} from "./domain/user";
import {HttpClient} from "@angular/common/http";
import {SimpleUser} from "./domain/simple-user";
import {Router} from "@angular/router";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  user: SimpleUser;

  logOut() {
    this.httpClient.post("/api/log-out", null)
      .subscribe(data => {
        this.user = null;
        this.navigateToLogIn();
      });
  }

  navigateToLogIn() {
    this.router.navigate(["log-in"]);
  }

  updateUser(): void {
    this.httpClient.get<SimpleUser>("/api/user")
      .subscribe(data => this.user = data, err => this.router.navigate(["/log-in"]));
  }

  ngOnInit(): void {
    this.updateUser();
    this.router.events.subscribe(val => this.updateUser());
  }
}
