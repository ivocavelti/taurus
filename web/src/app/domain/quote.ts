import {Quoter} from "./quoter";

export interface Quote {
  quoteId: number;
  quote: string;
  quoters: Quoter[];
}
