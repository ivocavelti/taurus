export interface Summary {
  username: string;
  question: number;
  totalQuestions: number;
  points: number;
}
