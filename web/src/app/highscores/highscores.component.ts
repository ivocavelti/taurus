import { Component, OnInit } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Score} from "../domain/score";
import {MatTableDataSource} from "@angular/material";
import {Router} from "@angular/router";

@Component({
  selector: 'app-highscores',
  templateUrl: './highscores.component.html',
  styleUrls: ['./highscores.component.scss']
})
export class HighscoresComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router) { }

  displayedColumns: string[] = ['username', 'score'];
  dataSource: MatTableDataSource<Score>;

  ngOnInit() {
    this.httpClient.get<Score[]>("/api/highscores")
      .subscribe(data => this.dataSource = new MatTableDataSource(data),
          error => this.router.navigate(["/log-in"]))
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

}
