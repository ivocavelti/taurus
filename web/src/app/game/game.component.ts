import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Quote} from "../domain/quote";
import {Quoter} from "../domain/quoter";
import {Summary} from "../domain/summary";
import {Router} from "@angular/router";

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.scss']
})
export class GameComponent implements OnInit {

  constructor(private httpClient: HttpClient, private router: Router) {
  }

  gameStarted = false;
  quote = new class implements Quote {
    quote: string;
    quoteId: number;
    quoters: Quoter[];
  };
  summary = new class implements Summary {
    points: number;
    question: number;
    totalQuestions: number;
    username: string;
  };


  ngOnInit() {
  }

  startGame() {
    this.httpClient.post<Summary>("/api/game/start", null)
      .subscribe((data) => {
        this.gameStarted = true;
        this.summary = data;
        this.getQuote();
      }, error => this.router.navigate(["/log-in"]))
  }

  getQuote() {
    this.httpClient.get<Quote>("/api/quote")
      .subscribe(data => this.quote = data,
          error => this.router.navigate(["/log-in"]));
  }

  makeGuess(id: number) {
    this.httpClient.post<Summary>("/api/quote/" + id, null)
      .subscribe(data => {
        this.summary = data;
        if(!(this.summary.question >= this.summary.totalQuestions)) {
          this.getQuote();
        }
      }, error => this.router.navigate(["/log-in"]))
  }
}
